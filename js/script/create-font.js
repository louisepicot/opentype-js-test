// 
opentype.load('./assets/fonts/Combinedd.otf', function (err, font) {
    if (err) {
        alert('Could not load font: ' + err);
    } else {

        let pathSvg,
            glyphArray = []
        const svg = document.getElementById("svg");
        const svg1 = document.getElementById("svg1");
        const svg2 = document.getElementById("svg2");

        let glyphsList = font.glyphs.glyphs

        document.querySelector("#partie1").addEventListener("mouseover", e => {
            setTimeout(() => {
                for (const property in glyphsList) {
                    glyphsList[property].path.commands.forEach(path => {
                        path.x += 5
                        path.y += 5
                    })
                    glyphArray.push(glyphsList[property])
                    let pathText = font.getPath(`Des questions d'orientation`, 0, 100, 50)
                    pathSvg = pathText.toSVG();
                    svg.innerHTML = " "
                    svg.insertAdjacentHTML("afterbegin", pathSvg)
                }
            }, 100);
        })

        document.querySelector("#partie2").addEventListener("mouseover", e => {
            setTimeout(() => {
                for (const property in glyphsList) {
                   
                    glyphsList[property].path.commands.forEach(path => {
                        path.x -= 5
                        path.y -= 5
                    })
                    glyphArray.push(glyphsList[property])
                    let pathText = font.getPath(`Des questions d'orientation`, 0, 100, 50)
                    pathSvg = pathText.toSVG();
                    svg1.innerHTML = " "
                    svg1.insertAdjacentHTML("afterbegin", pathSvg)
                }
            }, 100);
        })

        document.querySelector("#partie3").addEventListener("mouseover", e => {
            // console.log(e)
            setTimeout(() => {
                for (const property in glyphsList) {
                    // console.log(glyphsList[property])
                    glyphsList[property].path.commands.forEach(path => {
                        if (path.type === "C" && path.x > 550) {
                            console.log(path)
                            path.x += 2
                            path.y -=1
                            path.x1 += 10
                            path.y1 += 0
                            path.x2 += 2
                            path.y2 += 1
                            // path.y 
                            // console.log(path.x,
                            //     path.y)
                        }

                        // if (path.type === "M" && path.x > 550) {
                        //     console.log(path)
                        //     path.x -= 20
                        //     // path.y 
                        //     // console.log(path.x,
                        //     //     path.y)
                        // }

                    })
                    glyphArray.push(glyphsList[property])
                    let pathText = font.getPath(`Des questions d'orientation`, 0, 100, 50)
                    pathSvg = pathText.toSVG();
                    svg2.innerHTML = " "
                    svg2.insertAdjacentHTML("afterbegin", pathSvg)
                }
            }, 100);
        })
        // document.addEventListener("scroll", e => {
        //     for (const property in glyphsList) {
        //         glyphsList[property].path.commands.forEach(path => {
        //             path.x += 5
        //             path.y += 5
        //         })
        //         glyphArray.push(glyphsList[property])
        //         let pathText = font.getPath(`Des questions d'orientation`, 0, 100, 50)
        //         pathSvg = pathText.toSVG();
        //         svg.innerHTML = " "
        //         svg.insertAdjacentHTML("afterbegin", pathSvg)
        //     }
        // })



        // download font 

        document.querySelector("button").addEventListener('click', e => {
            // var glyphs = [notdefGlyph, glyphArray];
            var font = new opentype.Font({
                familyName: 'OpenTypeSans',
                styleName: 'Medium',
                unitsPerEm: 1000,
                ascender: 800,
                descender: -200,
                glyphs: glyphArray
            });
            font.download();
        })


    }
});